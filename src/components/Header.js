import React, {Component} from 'react';

class Header extends Component {
  render() {
    return (
        <div>

        <div className="logo">
          Budget app for Cybero
        </div>
        <div className="my_budget_div">
          My budget:
          <span id="my_budget">
            0 AZN
          </span>
        </div>
        <div id="error_divi" className="alert alert-danger" style={{
            display: 'none'
          }}>
          Please select type
        </div>

        <div id="success_divi" className="alert alert-success" style={{
            display: 'none'
          }}>
          Information added
        </div>



      </div>
    );
  }
}

export default Header;
