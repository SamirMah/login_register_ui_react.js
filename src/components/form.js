import React, {Component} from 'react';
import $ from 'jquery';

class Form extends Component {
  constructor(props) {
    super(props);

    this.initialState = {
      type: '',
      amount: '',
      description: '',
    };

    this.state = this.initialState;
  }

  handleChange = event => {
    const {name, value} = event.target;

    this.setState({[name]: value});
  }
  submitForm = event => {

    if (this.state.type !== "") {
      this.props.handleSubmit(this.state);
      this.setState(this.initialState);
       $('input[name=amount]').val('');
       $('input[name=description]').val('');
       $("#inputGroupSelect01").val($("#inputGroupSelect01 option:first").val());
       $('#success_divi').css('display' , 'block');
       $('#error_divi').css('display' , 'none');
    } else if (this.state.type == "") {
      $('input[name=amount]').val('');
      $('input[name=description]').val('');
      $("#inputGroupSelect01").val($("#inputGroupSelect01 option:first").val());
      $('#success_divi').css('display' , 'none');
     $('#error_divi').css('display' , 'block');
    }


    event.preventDefault();

  }
  render() {
    return (
          <form>
          <div className="input-group">
            <select name="type" onChange={this.handleChange} className="custom-select" id="inputGroupSelect01">
              <option>Type action</option>
              <option defaultValue="1">Income</option>
              <option defaultValue="2">Expense</option>

            </select>
            <input name="amount" type="number" onChange={this.handleChange} aria-label="First name" className="form-control" placeholder="Amount"/>
            <input name="description" type="text" aria-label="Last name" onChange={this.handleChange} className="form-control" placeholder="Description"/>
            <button type="submit" className="btn btn-primary" onClick={this.submitForm}>Add</button>
          </div>
          </form>
    );
  }
}

export default Form;
