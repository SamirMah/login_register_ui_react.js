import 'bootstrap/dist/css/bootstrap.min.css';
import React, {Component} from 'react';


import Form from './components/form';
import Budget_data from './Budget_data.jsx';
import Header from './components/Header';
import '../node_modules/font-awesome/css/font-awesome.min.css';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = { data:[]  };

  }

  handleSubmit = data => {
    this.setState({data: [...this.state.data, data]});
  }


  render() {
    return (<div className="App">
      <div className="header">
  <div className="add_buddget_section">
        <Header/>

        <Form handleSubmit={this.handleSubmit} />
        </div>

      </div>

      <Budget_data data={this.state.data}/>

      <span className="custom_design">
    <i className="fa fa-3x fa-meetup"></i>
</span>
    </div>);
  }
}

export default App;
