import React, {Component} from 'react';


class Budget_data extends Component {
  render() {

     let data = this.props.data;

     const incomes = data.map((data, index)=> {
        if (data.type === "Income") {
         return (
           <tr key={index}>
               <td>{data.amount}</td>
               <td>{data.description}</td>
               <td><button>Delete</button></td>
           </tr>)
       }
     });

     const expense = data.map((data, index)=> {
        if (data.type === "Expense") {
         return (
           <tr key={index}>
               <td>{data.amount}</td>
               <td>{data.description}</td>
               <td><button>Delete</button></td>
           </tr>)
       }
     });

    return (
        <div className="container">

 <div className="row">
          <div className="col-md-6">

            <h2> Income </h2>

            <table className="table" id="income">
              <thead>
                <tr>
                  <th scope="col">Amount</th>
                  <th scope="col">Description</th>
                    <th scope="col">Delete</th>
                </tr>
              </thead>
              <tbody>

                {incomes}

              </tbody>
            </table>

            <div className="total_div btn btn-primary">

              Total: <span id="total_income"> 0</span> AZN

            </div>

          </div>
          <div className="col-md-6">

            <h2> Expense </h2>

            <table className="table" id="expense">
              <thead>
                <tr>
                  <th scope="col">Amount</th>
                  <th scope="col">Description</th>
                  <th scope="col">Delete</th>
                </tr>
              </thead>
              <tbody>
                  {expense}
              </tbody>
            </table>

            <div className="total_div btn btn-danger">

              Total: <span id="total_expense"> 0</span> AZN

            </div>
          </div>
</div>
        </div>
    );
  }
}

export default Budget_data;
